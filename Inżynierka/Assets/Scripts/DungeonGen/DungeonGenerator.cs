﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonGenerator : MonoBehaviour {
    [SerializeField]
    private Room[] roomsArray;

    [SerializeField]
    private GameObject roomPrefab;
    [SerializeField]
    private PlayerMovement player;
    [SerializeField]
    private GameObject roomsParent;

    public Room[] GenerateRooms(int numberOfRooms)
    {
        for(int i = 0; i < numberOfRooms; i++)
        {
            var room = Instantiate(roomPrefab);
            room.GetComponent<Room>().roomNum = i;
            room.transform.SetParent(roomsParent.transform);
        }
        roomsArray = roomsParent.GetComponentsInChildren<Room>();
        return roomsArray;
    }

    public void GenerateDungeon(Graph g)
    {
        for(int i = 0; i < g.edges.Count; i++)
        {
            Node<Room> firstRoom = g.edges[i].from;
            Node<Room> secondRoom = g.edges[i].to;
            ConnectRooms(firstRoom, secondRoom);
        }
        player = FindObjectOfType<PlayerMovement>();
        player.transform.parent = g.startNode.data.transform;
        Camera.main.transform.parent = g.startNode.data.transform;
        player.transform.position = new Vector3(g.startNode.data.spawnPoint.position.x, g.startNode.data.spawnPoint.position.y, g.startNode.data.spawnPoint.position.z);
        Camera.main.transform.position = new Vector3(g.startNode.data.spawnPoint.position.x, g.startNode.data.spawnPoint.position.y, -10f);
        Camera.main.transform.Translate(new Vector3(0f, -0.9f, 0f));
        g.finishNode.data.SpawnFinalBoss();
    }

    public void ConnectRooms(Node<Room> first, Node<Room> second)
    {
        first.data.neighbours.Add(second.data);
        first.data.doorsCount++;
       
        DoorTrigger[] dt = first.data.GetComponentsInChildren<DoorTrigger>();
        for (int i = 0; i < dt.Length; i++)
        {
            if (dt[i].target == null)
            {
                dt[i].target = second.data;
                break;
            }
        }
    }
}
