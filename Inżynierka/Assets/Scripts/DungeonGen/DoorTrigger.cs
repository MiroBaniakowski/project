﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorTrigger : MonoBehaviour {

    public PlayerMovement player;
    public Room target;
    public Camera camera;
	// Use this for initialization
	void Start () {
        player = FindObjectOfType<PlayerMovement>();
        camera = Camera.main;
    }


    void OnTriggerEnter2D(Collider2D c)
    {
        if(c.name == "Player" && GetComponentInParent<Room>().Cleared())
        {
            GoThroughDoor();
        }
    }

    public void GoThroughDoor()
    {
        if(player != null && camera != null && target != null)
        {
            player.transform.position = new Vector3(target.spawnPoint.position.x, target.spawnPoint.position.y, target.spawnPoint.position.z);
            camera.transform.position = new Vector3(target.spawnPoint.position.x, target.spawnPoint.position.y, -10f);
            camera.transform.Translate(new Vector3(0f, -0.9f, 0f));
        }        
    }
}
