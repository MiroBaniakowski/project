﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Room : MonoBehaviour {
    [SerializeField]
    public int roomNum;
    [SerializeField]
    private Sprite[] woodSprites;

    [SerializeField]
    private Sprite[] stoneSprites;

    [SerializeField]
    private Sprite[] sandSprites;

    [SerializeField]
    private Sprite[] grassSprites;

    [SerializeField]
    private Sprite[] blueGround;

    [SerializeField]
    private Sprite[] doorSprites;

    [SerializeField]
    private int spritesCount = 5;

    [SerializeField]
    public SpriteRenderer[] sprites;
    [SerializeField]
    private GameObject _spritesParent;
    private Sprite selected;

    //tutaj się pojawiamy po wejściu z innego pokoju
    public Transform spawnPoint;

    public SpriteRenderer[] doors;
    public int doorsCount = 0;

    public SpriteRenderer[] walls;

    public PlayerMovement player;

    public List<Room> neighbours;

    [SerializeField]
    public Sprite wallTexture;

    [SerializeField]
    public GameObject[] enemies;

    public GameObject[] cannons;

    public GameObject finalBoss;

    public int spawnedEnemies = 0;

    public int minNumOfEnemies = 1;
    public int maxNumOfEnemies = 4;

    public float cannonChance = 0.33f;

	// Use this for initialization
	void Start () {
        sprites = _spritesParent.GetComponentsInChildren<SpriteRenderer>();
        SpawnEnemies();
        SpawnCannons();
        InitializeRoom();
        player = FindObjectOfType<PlayerMovement>();
	}
	
	// Update is called once per frame
	void Update () {
        UpdateDoors();
	}

    public void SpawnEnemies()
    {
        int numOfEnemies = Random.Range(minNumOfEnemies, maxNumOfEnemies + 1);
        for (int i = 0; i < numOfEnemies; i++)
        {
            GameObject enemy = Instantiate(enemies[Random.Range(0, enemies.Length)]);
            spawnedEnemies++;
            enemy.transform.SetParent(this.transform);
            enemy.transform.localPosition = spawnPoint.transform.position;
        }
    }

    public void SpawnFinalBoss()
    {
        GameObject boss = Instantiate(finalBoss);
        spawnedEnemies++;
        boss.transform.SetParent(this.transform);
        boss.transform.localPosition = spawnPoint.transform.position;
    }

    public void SpawnCannons()
    {
        for (int i = 0; i < cannons.Length; i++)
        {
            float r = Random.Range(0f, 1f);
            if (r <= cannonChance)
            {
                cannons[i].SetActive(true);
                spawnedEnemies++;
            }                
        }
    }

    private void SetSprites(int value)
    {
        for (int i = 0; i < sprites.Length; i++)
        {
            switch (value)
            {
                case 0:
                    selected = woodSprites[Random.Range(0, woodSprites.Length)];
                    break;
                case 1:
                    selected = stoneSprites[Random.Range(0, stoneSprites.Length)];
                    break;
                case 2:
                    selected = grassSprites[Random.Range(0, sandSprites.Length)];
                    break;
                case 3:
                    selected = sandSprites[Random.Range(0, grassSprites.Length)];
                    break;
                case 4:
                    selected = blueGround[Random.Range(0, blueGround.Length)];
                    break;
                default:
                    break;
            }
            if (sprites[i].gameObject.layer.ToString() != "Enemy")
                sprites[i].sprite = selected;
        }
    }

    public void InitializeRoom()
    {
        int selectedTexture = Random.Range(0, spritesCount);
        switch (selectedTexture)
        {
            case 0:
                SetSprites(0);
                break;
            case 1:
                SetSprites(1);
                break;
            case 2:
                SetSprites(2);
                break;
            case 3:
                SetSprites(3);
                break;
            case 4:
                SetSprites(4);
                break;
            default:
                break;
        }

        for(int i = 0; i < walls.Length; i++)
        {
            walls[i].sprite = wallTexture;
        }

        UpdateDoors();        
    }

    public void UpdateDoors()
    {
        for(int i = 0; i < doors.Length; i++)
        {
            if(doors[i].GetComponent<DoorTrigger>() != null)
            {
                if(doors[i].GetComponent<DoorTrigger>().target != null)
                {
                    if (Cleared())
                        doors[i].sprite = doorSprites[1];
                    else
                        doors[i].sprite = doorSprites[0];
                }                    
                else if(doors[i].GetComponent<DoorTrigger>().target == null)
                    doors[i].sprite = wallTexture;
            }
            else
                doors[i].sprite = wallTexture;
        }
    }

    public bool Cleared()
    {
        return (spawnedEnemies <= 0);
    }
}
