﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Graph : MonoBehaviour {
    [SerializeField]
    public List<Edge> edges;
    [SerializeField]
    public List<Node<Room>> nodes;
    [SerializeField]
    public Node<Room> startNode;
    [SerializeField]
    public Node<Room> finishNode;
    public Room[] rooms;
    public GameObject roomsGrid;
    public DungeonGenerator dunGenerator;
    public bool reachedEnd;
    public DecisionHandler decisionHandler;

    void Start()
    {
        decisionHandler = FindObjectOfType<DecisionHandler>();
        InformationHandler ih = FindObjectOfType<InformationHandler>();
        reachedEnd = false;
        dunGenerator = GetComponent<DungeonGenerator>();
        rooms = dunGenerator.GenerateRooms(ih.roomsNumber);    
        Generate(rooms, ih, 4);
        dunGenerator.GenerateDungeon(this);
    }

    void Update()
    {
        ReachedFinish();
    }

    public void AddEdge(Node<Room> a, Node<Room> b)
    {
        edges.Add(new Edge(a, b));
    }


    //WYBRAĆ STARTOWY I KOŃCOWY WIERZCHOŁEK
    public void Generate(Room[] rooms, InformationHandler ih, int maxEdgesPerNode)
    {
        nodes = new List<Node<Room>>();
        //lista pomocnicza, z której będziemy usuwać węzły(bo z nodes nie można)
        List<Node<Room>> disconnectedNodes = new List<Node<Room>>();
        List <Node<Room>> areInGraph = new List<Node<Room>>();
        nodes = new List<Node<Room>>(rooms.ToList().Select(room => new Node<Room>(room)));


        disconnectedNodes = new List<Node<Room>>(nodes);
        
        startNode = nodes[Random.Range(0, nodes.Count)];
        areInGraph.Add(startNode);
        disconnectedNodes.Remove(startNode);
        finishNode = disconnectedNodes[Random.Range(0, disconnectedNodes.Count)];

        while (!AreAllInGraph())
        {
            Node<Room> inGraph = areInGraph[Random.Range(0, areInGraph.Count)];
            Node<Room> notInGraph = disconnectedNodes[Random.Range(0, disconnectedNodes.Count)];
            if(!(inGraph.numOfNeighbours >= maxEdgesPerNode))
            {
                AddEdge(inGraph, notInGraph);
                AddEdge(notInGraph, inGraph);
                inGraph.numOfNeighbours++;
                notInGraph.numOfNeighbours++;
                disconnectedNodes.Remove(notInGraph);
                areInGraph.Add(notInGraph);
            }                
        }
        Debug.Log("Przed: ");
        WriteGraph();
    }

    public bool AreAllInGraph()
    {
        return !nodes.Exists(node => node.numOfNeighbours == 0);
    }

    public void ReachedFinish()
    {
        if (finishNode.data.Cleared() && !reachedEnd)
        {
            decisionHandler.PauseGame();
            reachedEnd = true;
        }

    }

    public void WriteGraph()
    {
        for(int i = 0; i < edges.Count; i++)
        {
            Debug.Log("From: {" + edges[i].from.data.roomNum + "} to: {" + edges[i].to.data.roomNum + "}");
        }
    }
}

