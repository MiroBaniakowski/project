﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Node<T> {

    [SerializeField]
    public T data;

    public int numOfNeighbours;
    public Node(T d){
        data = d;
        numOfNeighbours = 0;
    }
}
