﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Edge : MonoBehaviour
{

    [SerializeField]
    public Node<Room> from;
    [SerializeField]
    public Node<Room> to;

    public Edge(Node<Room> from, Node<Room> to)
    {
        this.from = from;
        this.to = to;
    }
}