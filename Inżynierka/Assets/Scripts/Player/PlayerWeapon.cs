﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerWeapon : MonoBehaviour {
    public Weapon equipped;

    public Transform firePoint;
    public GameObject[] bulletPrefabs;
    public GameObject selected;
    public Text nameText;
    private float nextFire = 0.0f;

    // Use this for initialization
    void Start () {
        selected = bulletPrefabs[0];
        equipped.GetComponent<Gun>().bulletPrefab = selected;
        nameText = FindObjectOfType<PanelOpener>().GetComponentInChildren<Text>();
        nameText.text = ("Wyposażona broń: " + equipped.GetComponent<Gun>().bulletPrefab.GetComponent<Bullet>().weaponName);
    }

    // Update is called once per frame
    void Update()
    {
       
        if (Input.GetButtonDown("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + equipped.GetComponent<Gun>().bulletPrefab.GetComponent<Bullet>().timeBetweenShoots;
            StartCoroutine(equipped.Attack());
        }

        if(Input.GetKeyDown(KeyCode.Q))
        {
            Debug.Log(equipped.GetComponent<Gun>().bulletPrefab.GetComponent<Bullet>().weaponName);
            SwitchWeapons();
            nameText.text = ("Wyposażona broń: " + equipped.GetComponent<Gun>().bulletPrefab.GetComponent<Bullet>().weaponName);
        }
        //Debug.Log(equipped._timeBetweenShoots);
    }

    private void SwitchWeapons()
    {
        for(int i = 0; i < bulletPrefabs.Length; i++)
        {
            if(selected == bulletPrefabs[i])
            {
                if(i + 1 < bulletPrefabs.Length)
                {
                    selected = equipped.GetComponent<Gun>().bulletPrefab;
                    equipped.GetComponent<Gun>().bulletPrefab = bulletPrefabs[i + 1];
                }
                else if(i + 1 == bulletPrefabs.Length)
                {
                    equipped.GetComponent<Gun>().bulletPrefab = bulletPrefabs[0];
                    selected = bulletPrefabs[0];
                }
            }
        }
        
    }	
}
