﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Healing : MonoBehaviour {
    public int healthToRestore = 100;
	
	void OnTriggerEnter2D(Collider2D other)
    {
        if(other.name == "Player")
        {
            FindObjectOfType<AudioManager>().Play("HPCollect");
            other.GetComponent<PlayerHealth>().Heal(healthToRestore);
            Destroy(gameObject);
        }
    }
}
