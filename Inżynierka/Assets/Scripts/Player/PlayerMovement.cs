﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour {
    [SerializeField]
    private float movementSpeed = 3f;

    public Rigidbody2D rb;

    public Animator anim;

    [SerializeField]
    private Camera cam;

    Vector2 movement;
    Vector2 mousePos;

	// Use this for initialization
	void Start () {
        cam = Camera.main;
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");
        if(movement.x > 0.5f || movement.x < -0.5f || movement.y > 0.5f || movement.y < -0.5f)
        {
            anim.SetBool("IsMoving", true);
        }
        else
        {
            anim.SetBool("IsMoving", false);
        }
        
        mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
    }

    void FixedUpdate()
    {
        rb.MovePosition(rb.position + movement * movementSpeed * Time.deltaTime * Time.timeScale);
        Vector2 lookDir = mousePos - rb.position;
        //kąt pomiędzy pozycją myszki, a graczem(z radianów na stopnie)
        float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg - 90f;
        rb.rotation = angle;
    }

    
}
