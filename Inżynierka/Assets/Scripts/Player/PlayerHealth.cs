﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {

    [SerializeField]
    private int _maxHealth = 100;
    [SerializeField]
    private int _currentHealth = 100;
    [SerializeField]
    private Image hpBar;

    public bool immuneToDamage;

    public float immunityTime = 1.5f;

    public GameObject deathEffect;

    public DecisionHandler decisionHandler;

	// Use this for initialization
	void Start () {
        immuneToDamage = true;
        StartCoroutine(GrantImmunity());
	}
	
	// Update is called once per frame
	void Update () {
        if(hpBar != null)
            hpBar.fillAmount = (float)_currentHealth / _maxHealth;
	}

    public IEnumerator DealDamage(int amount)
    {
        if (!immuneToDamage)
        {
            _currentHealth -= amount;
            if (_currentHealth < 0)
            {
                Die();
            }
        }        
        yield return new WaitForSeconds(1f);
    }

    public void Heal(int amount)
    {
        if (_currentHealth + amount >= _maxHealth)
            _currentHealth = _maxHealth;
        else
            _currentHealth += amount;
    }

    public IEnumerator GrantImmunity()
    {
        yield return new WaitForSeconds(immunityTime);
        immuneToDamage = false;
    }

    public void Die()
    {
        Instantiate(deathEffect, transform.position, Quaternion.identity);
        Destroy(gameObject);
        decisionHandler.PauseGame();
    }
}
