﻿using UnityEngine;

public class PanelOpener : MonoBehaviour {
    [SerializeField]
    private GameObject inventoryPanel;
    [SerializeField]
    private GameObject characterPanel;
    [SerializeField]
    private KeyCode keyCode;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(keyCode))
        {
            inventoryPanel.GetComponent<CanvasGroup>().alpha = (inventoryPanel.GetComponent<CanvasGroup>().alpha == 0f ? 1f : 0f);
            inventoryPanel.GetComponent<CanvasGroup>().blocksRaycasts = (inventoryPanel.GetComponent<CanvasGroup>().blocksRaycasts == true ? false : true);
        }
	}
}
