﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeeleEnemy : Enemy {
    private Vector3 direction;
    public float timeBetweenAttacks = 0.0f;
    public float timeCounter = 0.0f;

    // Use this for initialization
	public override void Start () {
        base.Start();        
        MovementSpeed = 1f;
	}
	
	// Update is called once per frame
	void Update () {
        if(player == null)
        {
            return; 
        }
        direction = player.position - transform.position;
        //Kąt
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;
        Rigidbody.rotation = angle;
    }

    void FixedUpdate()
    {
        Move();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.name == "Player" && Time.time > timeCounter)
        {
            timeCounter = Time.time + timeBetweenAttacks;
            StartCoroutine(other.GetComponent<PlayerHealth>().DealDamage(Damage));
        }
    }

    public override void Move()
    {
        if(player != null)
        {
            direction = player.position - transform.position;
            direction.Normalize();
            Rigidbody.MovePosition(transform.position + (direction * MovementSpeed * Time.deltaTime * Time.timeScale));
            anim.SetBool("IsMoving", true);
        }
    }

}
