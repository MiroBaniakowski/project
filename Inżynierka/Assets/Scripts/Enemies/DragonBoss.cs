﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonBoss : RangedEnemy {

    public Transform[] shootingPoints;
    public GameObject fireBallPrefab;
    // Use this for initialization
    public override void Start () {
        base.Start();
        shootingPoints = GetComponentsInChildren<Transform>();
        timeToNextShoot = timeBetweenShoots;
        player = FindObjectOfType<PlayerHealth>().transform;
        MovementSpeed = 3f;
	}
	
	// Update is called once per frame
	void Update () {
        Move();
        if(timeToNextShoot <= 0)
        {
            Attack();
            timeToNextShoot = timeBetweenShoots;
        }
        else
        {
            timeToNextShoot -= Time.deltaTime * Time.timeScale;
        }
	}

    public override void Attack()
    {
        for(int i = 0; i < shootingPoints.Length; i++)
        {
            GameObject bullet = Instantiate(fireBallPrefab, shootingPoints[i].position, Quaternion.identity);        
        }
    }

    public override void Move()
    {
        base.Move();
    }
}
