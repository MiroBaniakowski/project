﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemy : MonoBehaviour {

    public int currentHP;

    public int maxHP;

    private float movementSpeed;

    public float chanceToSpawnHealth = 0.2f;

    private PlayerHealth playerHealth;
    [SerializeField]
    private Rigidbody2D rigidBody;

    public Transform player;

    private int _damage = 5;

    public Animator anim;

    public GameObject healthItem;

    public GameObject destructionEffect;

    public AudioManager am;
    public int Damage
    {
        get { return _damage; }
        set { _damage = value; }
    }

    public int CurrentHP
    {
        get { return currentHP; }
        set { currentHP = value; }
    }

    public int MaxHP
    {
        get { return maxHP; }
        set { maxHP = value; }
    }

    public float MovementSpeed
    {
        get { return movementSpeed; }
        set { movementSpeed = value; }
    }

    public PlayerHealth PlayerHealth
    {
        get { return playerHealth; }
    }

    public Rigidbody2D Rigidbody
    {
        get { return rigidBody; }
        set { rigidBody = value; }
    }

    public virtual void Attack()
    {

    }

    public virtual void Move()
    {

    }

    public virtual void Die()
    {
        GetComponentInParent<Room>().spawnedEnemies--;
        Destroy(gameObject);
        if (destructionEffect != null)
        {
            Instantiate(destructionEffect, transform.position, Quaternion.identity);
        }
            
        float r = Random.Range(0f, 1f);
        if(r <= chanceToSpawnHealth)
            Instantiate(healthItem, transform.position, Quaternion.identity);
    }

    public virtual void DealDamageToMonster(int amount)
    {
        currentHP -= amount;
        if(currentHP <= 0)
        {
            Die();
        }
    }

	// Use this for initialization
	public virtual void Start () {
		playerHealth = FindObjectOfType<PlayerHealth>();
        rigidBody = GetComponent<Rigidbody2D>();
        am = FindObjectOfType<AudioManager>();
        player = playerHealth.transform;
        //healthItem = GameObject.Find("BonusHP");
        //destructionEffect = GameObject.Find("CannonExplosionEffect");
    }
}
