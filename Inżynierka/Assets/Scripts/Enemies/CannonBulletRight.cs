﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBulletRight : CannonBullet {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	public override void Update () {
        rb.velocity = transform.right * speed;
	}
}
