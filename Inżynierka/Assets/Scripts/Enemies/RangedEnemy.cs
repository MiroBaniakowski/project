﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedEnemy : Enemy {

    public Transform firePoint;
    //Stworzyć oddzielny prefab i skrypt
    public GameObject enemyBulletPrefab;

    public float timeToNextShoot = 2f;
    public float timeBetweenShoots;

    public float stoppingDistance = 20f;

    public float retreatDistance = 10f;

    public float maximumShootDistance = 5f;

    public float offset = 90f;
    Vector3 direction;
    
	// Use this for initialization
	public override void Start () {
        base.Start();
        MovementSpeed = 2f;
        timeToNextShoot = timeBetweenShoots;
    }
	
	// Update is called once per frame
	void Update () {
        Move();
        if(timeToNextShoot <= 0)
        {
            if (AbleToShoot())
            {
                Attack();
            }
            timeToNextShoot = timeBetweenShoots;
        }
        else
        {
            timeToNextShoot -= Time.deltaTime * Time.timeScale;
        }
	}

    public override void Attack()
    {
        Instantiate(enemyBulletPrefab, firePoint.position, Quaternion.identity);
        am.Play("GunShoot");
    }

    public override void Move()
    {
        if (player != null)
        {
            direction = player.position - transform.position;
            //Kąt
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - offset;
            Rigidbody.rotation = angle;
            float distance = Vector2.Distance(transform.position, player.position);
            if (distance > stoppingDistance)
            {
                transform.position = Vector2.MoveTowards(transform.position, player.position, MovementSpeed * Time.deltaTime * Time.timeScale);
            }
            else if (distance < stoppingDistance &&
                Vector2.Distance(transform.position, player.position) > retreatDistance)
            {
                transform.position = this.transform.position;
            }
            else if (distance < retreatDistance)
            {
                transform.position = Vector2.MoveTowards(transform.position, player.position, -MovementSpeed * Time.deltaTime * Time.timeScale);
            }
        }
        anim.SetBool("IsMoving", true);
    }

    public bool AbleToShoot()
    {
        if(player != null)
        {
            if (Vector2.Distance(transform.position, player.position) <= maximumShootDistance)
            {
                return true;
            }            
        }
        return false;
    }
}
