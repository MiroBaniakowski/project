﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonBullet : EnemyBullet {

	// Use this for initialization
	public override void Start () {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = Vector3.up * speed * Time.deltaTime * Time.timeScale;
    }
}
