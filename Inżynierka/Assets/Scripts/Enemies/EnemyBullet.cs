﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour {
    public float speed = 6f;
    public int damage = 5;

    public GameObject explosionEffect;
    public Transform player;
    protected Rigidbody2D rb;
    private Vector2 target;

	// Use this for initialization
	public virtual void Start () {
        rb = GetComponent<Rigidbody2D>();
        player = FindObjectOfType<PlayerMovement>().transform;
        target = (player.position - transform.position).normalized * speed;
        rb.velocity = new Vector2(target.x, target.y);
    }
	
    public virtual void OnCollisionEnter2D(Collision2D col)
    {
        if(col.collider.tag != "Enemy")
        {
            if (col.collider.GetComponent<PlayerHealth>() != null)
            {
                StartCoroutine(col.collider.GetComponent<PlayerHealth>().DealDamage(damage));
            }
            Destroy(gameObject);
            Instantiate(explosionEffect, transform.position, Quaternion.identity);
        }        
    }
}
