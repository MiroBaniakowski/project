﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBullet : MonoBehaviour {

    public Rigidbody2D rb;
    public Animator anim;
    public GameObject explosionEffect;
    public float speed = 3f;
    public int damage = 4;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
        //anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	public virtual void Update () {
        //rb.velocity = transform.up * speed;
	}

    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.tag != "Enemy")
        {
            if (collision.collider.GetComponent<PlayerHealth>() != null)
            {
                StartCoroutine(collision.collider.GetComponent<PlayerHealth>().DealDamage(damage));
            }
            Instantiate(explosionEffect, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }        
    }
}
