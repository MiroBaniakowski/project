﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : Enemy {
    public GameObject topBulletPrefab;
    public GameObject bottomBulletPrefab;
    public GameObject leftBulletPrefab;
    public GameObject rightBulletPrefab;
    public List<Transform> firePoints;
    public float timeBetweenShoots = 3f;
    public float startTimeBetweenShots;
    public int firstCannon;
    public int secondCannon;
    public GameObject destroyedCannon;

    // Use this for initialization
    public override void Start () {
        startTimeBetweenShots = timeBetweenShoots;
        firePoints.AddRange(GetComponentsInChildren<Transform>());
        firePoints.Remove(transform);
        am = FindObjectOfType<AudioManager>();
	}
	
	// Update is called once per frame
	void Update () {
        if (timeBetweenShoots <= 0)
        {
            am.Play("CannonShoot");
            Attack();
            timeBetweenShoots = startTimeBetweenShots;
        }
        else
        {
            timeBetweenShoots -= Time.deltaTime * Time.timeScale;
        }
    }

    public override void Attack()
    {
        switch (firstCannon)
        {
            case -1:
                Instantiate(bottomBulletPrefab, firePoints[1].position, Quaternion.identity);
                break;
            case 1:
                Instantiate(topBulletPrefab, firePoints[1].position, Quaternion.identity);
                break;
        }
        switch (secondCannon)
        {
            case -1:
                Instantiate(leftBulletPrefab, firePoints[0].position, Quaternion.identity);
                break;
            case 1:
                Instantiate(rightBulletPrefab, firePoints[0].position, Quaternion.identity);
                break;
        }
    }

    public override void Die()
    {
        base.Die();
        Instantiate(destroyedCannon, transform.position, Quaternion.identity);
        am.Play("CannonExplosion");
    }
}
