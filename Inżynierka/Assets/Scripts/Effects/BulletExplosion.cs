﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletExplosion : MonoBehaviour {
	public void DestroyBullet()
    {
        Destroy(gameObject);
    }
}
