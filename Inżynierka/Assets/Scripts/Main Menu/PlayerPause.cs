﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPause : MonoBehaviour {

    public DecisionHandler dh;
    bool isMenuUp = false;
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Escape))
        {
            if (!isMenuUp)
            {
                dh.PauseGame();
                isMenuUp = true;
            }
            else
            {
                dh.ContinueGame();
                isMenuUp = false;
            }            
        }
	}
}
