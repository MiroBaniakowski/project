﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class GenerateButton : MonoBehaviour {

    public InformationHandler ih;
    private Button button;
    public InputField input;
    public GameObject warningPanel;
    public Animator anim;

	// Use this for initialization
	void Start () {
        button = GetComponent<Button>();
        button.onClick.AddListener(Load);
	}
	
	private IEnumerator LoadLevel()
    {
        int n;
        if(int.TryParse(input.text, out n))
        {
            if (int.Parse(input.text) > 1)
            {
                ih.roomsNumber = int.Parse(input.text);
                anim.SetTrigger("Start");
                yield return new WaitForSeconds(1f);
                SceneManager.LoadScene("MainWorld");
            }               
            else
            {
                warningPanel.SetActive(true);
                yield return null;
            }            
        }
        else
        {
            warningPanel.SetActive(true);
        }
    }

    public void Load()
    {
        StartCoroutine(LoadLevel());
    }
}
