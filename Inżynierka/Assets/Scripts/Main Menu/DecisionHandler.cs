﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class DecisionHandler : MonoBehaviour {

    public Button continueButton;
    public Button menuButton;


	// Use this for initialization
	void Start () {
        if(menuButton != null)
            menuButton.onClick.AddListener(LoadMainMenu);
        if(continueButton != null)
            continueButton.onClick.AddListener(ContinueGame);
	}

    public void LoadMainMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("MainMenu");
        AudioManager am = FindObjectOfType<AudioManager>();
        foreach(Sound s in am.sounds)
        {
            s.source.Stop();
        }
    }

    public void PauseGame()
    {
        Time.timeScale = 0.000001f;
        GetComponent<CanvasGroup>().alpha = 1f;
        GetComponent<CanvasGroup>().interactable = true;
        GetComponent<CanvasGroup>().blocksRaycasts = true;
    }

    public void ContinueGame()
    {

        Time.timeScale = 1f;
        GetComponent<CanvasGroup>().alpha = 0f;
        GetComponent<CanvasGroup>().interactable = false;
        GetComponent<CanvasGroup>().blocksRaycasts = false;
    }
}
