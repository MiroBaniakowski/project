﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public string weaponName;
    public float speed = 20f;
    public int damage = 40;
    public Rigidbody2D rb;
    public GameObject impactEffect;
    public float timeBetweenShoots = 0.5f;

    // Use this for initialization
    void Start()
    {
        Physics2D.IgnoreCollision(GetComponent<Collider2D>(), FindObjectOfType<PlayerMovement>().GetComponent<Collider2D>());
    }

    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.name != "Player")
        {
            Enemy enemy = collision.collider.GetComponent<Enemy>();
            if (enemy != null)
            {
                enemy.DealDamageToMonster(damage);
            }

            Instantiate(impactEffect, transform.position, transform.rotation);
            Destroy(gameObject);
        }        
    }

}