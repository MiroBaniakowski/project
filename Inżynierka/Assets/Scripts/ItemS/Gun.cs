﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Gun : Weapon {
    public Transform firePoint;
    public GameObject bulletPrefab;

    public override IEnumerator Attack()
    {
        base.Attack();
        audioManager.Play("GunShoot");
        GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
        rb.AddForce(firePoint.up * bullet.GetComponent<Bullet>().speed, ForceMode2D.Impulse);
        yield return new WaitForSeconds(_timeBetweenShoots);
    }
}
