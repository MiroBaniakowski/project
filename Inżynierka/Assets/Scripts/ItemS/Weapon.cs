﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {

    private string _name;
    private int _damage;
    [SerializeField]
    public float _timeBetweenShoots;
    public AudioManager audioManager;

    public string Name
    {
        get { return _name; }
        set { _name = value; }
    }

    public int Damage
    {
        get { return _damage; }
        set { _damage = value; }
    }

	// Use this for initialization
	void Start () {
        audioManager = FindObjectOfType<AudioManager>();
	}
	
    public virtual IEnumerator Attack()
    {
        yield return new WaitForSeconds(_timeBetweenShoots);
    }
}
