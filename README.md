## Table of contents
* [Requirements](#Requirements)
* [Installation](#Installation)
* [Launch](#Launch)

# Requirements:
1. Unity 2018.2.19.f1 (only to view the project, not required for built version).
2. 64-bit Windows operating system.
3. Visual Studio 2019.

# Installation:
```
git clone https://bitbucket.org/MiroBaniakowski/project/src/master/
```
This will download both the Unity project, and built version, that is ready to launch.

# Launch:
To launch already built version of the application, use Inż\project\Wersja zbudowana\Inżynierka.exe file.
To open the project in Unity, locate Inż\project\Inżynierka\Assets\Scenes\MainMenu.unity, or Inż\project\Inżynierka\Assets\Scenes\MainWorld.unity  file.
